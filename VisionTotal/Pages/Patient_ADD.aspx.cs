﻿using System;
using System.Data;
using VisionTotal.Controllers;

namespace VisionTotal.Pages
{
    public partial class Patient_ADD : System.Web.UI.Page
    {

        public string Error = "";
        public string Success = "";
        bool bError, bSuccess;
        PatientController Patient = new PatientController();
        Models.Patients PatientData = new Models.Patients();
        protected void MostrarMensaje()
        {
            if (bError)
            {
                DivError.Visible = true;
            }
            else if (bSuccess)
            {
                DivSuccess.Visible = true;
            }
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            DateTime DateOfBirth = new DateTime();
            DateTime.TryParse(txtDateOfBirth.Text, out DateOfBirth);
            PatientData = new Models.Patients
            {
                Nombre = txtName.Text,
                ApellidoPaterno = txtLastname.Text,
                ApellidoMaterno = txtLastname2.Text,
                Calle = txtStreet.Text,
                NumExt = txtExtNumber.Text,
                NumInt = txtIntNumber.Text,
                Colonia = txtDistrict.Text,
                Genero_ID = Convert.ToInt16(dropGender.SelectedValue),
                EstadoCivil_ID = Convert.ToInt16(dropMaritalStatus.SelectedValue),
                Email = txtEmail.Text,
                Telefono = txtTelephone.Text,
                Celular = txtCellphone.Text,
                Trabajo_Direccion = txtWorkplaceAddress.Text,
                Referencia = txtRefference.Text,
                Seguro_Medico = txtHealthInsurance.Text,
                FechaNacimiento = DateOfBirth,
            };


            if (string.IsNullOrEmpty(PatientData.Nombre) || string.IsNullOrEmpty(PatientData.ApellidoPaterno))
            {
                bError = true;
                Error = "<span  class='alert-link'>Error</span><br /><span>Los campos de nombre y apellido paterno, son campos requeridos para dar de alta al paciente. </span>";

            }
            else
            {
                DataTable results = Patient.InsertWithResult(PatientData);
                if (results != null)
                {
                    Response.Redirect("Patient.aspx?ID=" + results.Rows[0]["Paciente_ID"].ToString());
                }
                else
                {
                    bError = true;
                    Error = "<span  class='alert-link'>Error</span><br /><span>Ocurrio Un error con la comunicación. Inténtelo de nuevo. </span>";
                }
            }
            MostrarMensaje();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DivError.Visible = false;
            DivSuccess.Visible = false;
            if (!IsPostBack)
            {
                dropGender.DataTextField = "Genero";
                dropGender.DataValueField = "Genero_ID";
                dropGender.DataSource = Patient.ViewGenderList();
                dropGender.DataBind();

                dropMaritalStatus.DataTextField = "EstadoCivil";
                dropMaritalStatus.DataValueField = "EstadoCivil_ID";
                dropMaritalStatus.DataSource = Patient.ViewMaritalStatusList();
                dropMaritalStatus.DataBind();
            }
            MostrarMensaje();
        }
    }
}