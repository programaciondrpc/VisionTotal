﻿using System;
using System.Data;
using VisionTotal.Controllers;

namespace VisionTotal.Pages
{
    public partial class Patient_Update : System.Web.UI.Page
    {
        public string Error = "";
        public string Success = "";
        public string ID = "";
        bool bError, bSuccess;
        PatientController Patient = new PatientController();
        Models.Patients PatientData = new Models.Patients();
        protected void Page_Load(object sender, EventArgs e)
        {
            DivError.Visible = false;
            DivSuccess.Visible = false;
            string url = "Patients.aspx";
            if (Request.QueryString["ID"] == null)
            {
                Response.Redirect(url);
            }
            int id;
            int.TryParse(Request.QueryString["ID"], out id);
            if (id < 1)
            {
                Response.Redirect(url);
            }
            if (!IsPostBack)
            {
                ID = id.ToString();
                dropGender.DataTextField = "Genero";
                dropGender.DataValueField = "Genero_ID";
                dropGender.DataSource = Patient.ViewGenderList();
                dropGender.DataBind();

                dropMaritalStatus.DataTextField = "EstadoCivil";
                dropMaritalStatus.DataValueField = "EstadoCivil_ID";
                dropMaritalStatus.DataSource = Patient.ViewMaritalStatusList();
                dropMaritalStatus.DataBind();

                DataTable results = Patient.ViewPatient(id);
                if (results.Rows.Count < 1)
                {
                    Response.Redirect(url);
                }
                DateTime DateOfbirth;
                DateTime.TryParse(results.Rows[0]["FechaNacimiento"].ToString(), out DateOfbirth);
                txtName.Text = results.Rows[0]["Nombre"].ToString();
                txtLastname.Text = results.Rows[0]["ApellidoPaterno"].ToString();
                txtLastname2.Text = results.Rows[0]["ApellidoMaterno"].ToString();
                txtStreet.Text = results.Rows[0]["Calle"].ToString();
                txtDistrict.Text = results.Rows[0]["Colonia"].ToString();
                txtIntNumber.Text = results.Rows[0]["NumInt"].ToString();
                txtExtNumber.Text = results.Rows[0]["NumExt"].ToString();
                txtTelephone.Text = results.Rows[0]["Telefono"].ToString();
                txtCellphone.Text = results.Rows[0]["Celular"].ToString();
                dropGender.SelectedValue = results.Rows[0]["Genero_ID"].ToString();
                txtDateOfBirth.Text = DateOfbirth.ToString("MM/dd/yyyy");
                dropMaritalStatus.SelectedValue = results.Rows[0]["EstadoCivil_ID"].ToString();
                txtEmail.Text = results.Rows[0]["Email"].ToString();
                txtWorkplaceAddress.Text = results.Rows[0]["Trabajo_Direccion"].ToString();
                txtRefference.Text = results.Rows[0]["Referencia"].ToString();
                txtHealthInsurance.Text = results.Rows[0]["Seguro_Medico"].ToString();
            }
            MostrarMensaje();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Request.QueryString["ID"]);
            DateTime DateOfBirth = new DateTime();
            DateTime.TryParse(txtDateOfBirth.Text, out DateOfBirth);
            PatientData = new Models.Patients
            {
                Paciente_ID = id,
                Nombre = txtName.Text,
                ApellidoPaterno = txtLastname.Text,
                ApellidoMaterno = txtLastname2.Text,
                Calle = txtStreet.Text,
                NumExt = txtExtNumber.Text,
                NumInt = txtIntNumber.Text,
                Colonia = txtDistrict.Text,
                Genero_ID = Convert.ToInt16(dropGender.SelectedValue),
                EstadoCivil_ID = Convert.ToInt16(dropMaritalStatus.SelectedValue),
                Email = txtEmail.Text,
                Telefono = txtTelephone.Text,
                Celular = txtCellphone.Text,
                Trabajo_Direccion = txtWorkplaceAddress.Text,
                Referencia = txtRefference.Text,
                Seguro_Medico = txtHealthInsurance.Text,
                FechaNacimiento = DateOfBirth,
            };


            if (string.IsNullOrEmpty(PatientData.Nombre) || string.IsNullOrEmpty(PatientData.ApellidoPaterno))
            {
                bError = true;
                Error = "<span  class='alert-link'>Error</span><br /><span>Los campos de nombre y apellido paterno, son campos requeridos para guardar los datos del paciente. </span>";

            }
            else
            {
                bool result = Patient.Update(PatientData);
                if (!result)
                {
                    bSuccess = true;
                    Success = "<span  class='alert-link'>Éxito</span><br /><span>Los datos del paciente fueron guardados correctamente. </span>";
                }
                else
                {
                    bError = true;
                    Error = "<span  class='alert-link'>Error</span><br /><span>Ocurrio Un error con la comunicación. Inténtelo de nuevo. </span>";
                }
            }
            MostrarMensaje();

        }

        protected void MostrarMensaje()
        {
            if (bError)
            {
                DivError.Visible = true;
            }
            else if (bSuccess)
            {
                DivSuccess.Visible = true;
            }
        }

    }
}
