﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterTemplate.Master" AutoEventWireup="true" CodeBehind="Patient_ADD.aspx.cs" Inherits="VisionTotal.Pages.Patient_ADD" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
    Paciente
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Subtitle" runat="server">
    Agregar
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Body" runat="server">
    <div class="alert alert-danger" id="DivError" runat="server" role="alert">
        <%Response.Write(Error); %>
    </div>
    <div class="alert alert-success" id="DivSuccess" runat="server" role="alert">
        <%Response.Write(Success); %>
    </div>
    <div class="row">
        <div class="col-xs-12 text-left">
            <a href="Patients.aspx" title="Regresar...">
                <div class="col-xs-1 col-sm-1 col-md-1  col-lg-1">
                    <i class="fa fa-long-arrow-left fa-2x visible-xs" aria-hidden="true"></i>
                    <i class="fa fa-long-arrow-left fa-3x visible-sm" aria-hidden="true"></i>
                    <i class="fa fa-long-arrow-left fa-4x hidden-xs hidden-sm" aria-hidden="true"></i>
                </div>
                <div class="col-xs-10 col-sm-3 col-md-3  col-lg-3">
                    <span class="visible-xs" style="font-size: large; vertical-align: text-top;">Regresar</span>
                    <span class="visible-sm" style="font-size: x-large; vertical-align: text-top;">Regresar</span>
                    <span class="hidden-xs hidden-sm" style="font-size: xx-large; vertical-align: text-top;">Regresar</span>
                </div>
            </a>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" style="font-size: 18px;">Datos</div>
        <div class=" panel-body">
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Nombre:*</label>
                        <asp:TextBox ID="txtName" placeholder="Nombre" CssClass=" form-control text-capitalize" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Apellido paterno:*</label>
                        <asp:TextBox ID="txtLastname" placeholder="Apellido paterno" CssClass=" form-control text-capitalize" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Apellido materno:</label>
                        <asp:TextBox ID="txtLastname2" placeholder="Apellido materno" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label>Calle:</label>
                        <asp:TextBox ID="txtStreet" CssClass="form-control text-capitalize" placeholder="Calle" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label>No. Exterior:</label>
                        <asp:TextBox CssClass="form-control" ID="txtExtNumber" placeholder="No. Exterior" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label>No. Interior:</label>
                        <asp:TextBox CssClass="form-control" ID="txtIntNumber" placeholder="No. Interior" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label>Colonia:</label>
                        <asp:TextBox ID="txtDistrict" placeholder="Colonia" CssClass=" form-control text-capitalize" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Teléfono:</label>
                        <asp:TextBox ID="txtTelephone" placeholder="Teléfono" CssClass=" form-control  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Celular:</label>
                        <asp:TextBox ID="txtCellphone" placeholder="Celular" CssClass=" form-control  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label>Genero:</label>
                        <asp:DropDownList ID="dropGender" CssClass="form-control text-capitalize" runat="server"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Fecha de nacimiento:</label>
                        <asp:TextBox ID="txtDateOfBirth" placeholder="Fecha de nacimiento" CssClass=" form-control datepicker " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label>Estado Civil:</label>
                        <asp:DropDownList ID="dropMaritalStatus" CssClass="form-control" runat="server"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>E-mail:</label>
                        <asp:TextBox ID="txtEmail" placeholder="E-mail" CssClass=" form-control text-lowercase  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Dirección de su trabajo:</label>
                        <asp:TextBox ID="txtWorkplaceAddress" placeholder="Dirección de su trabajo" CssClass=" form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Quien lo refirio con nosotros:</label>
                        <asp:TextBox ID="txtRefference" placeholder="Referencia" CssClass=" form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Seguro medico:</label>
                        <asp:TextBox ID="txtHealthInsurance" placeholder="Seguro medico" CssClass=" form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>


        </div>
        <div class=" panel-footer  text-right">
            <asp:Button ID="btnRegister" runat="server" Text=" Registrar" CssClass="btn  btn-default text-uppercase" OnClick="btnRegister_Click" />
        </div>
    </div>
</asp:Content>