﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisionTotal.Controllers;

namespace VisionTotal.Pages
{
    public partial class Patient_Consult : System.Web.UI.Page
    {
        public string Error = "";
        public string Success = "";
        public string ID = "";
        bool bError, bSuccess;
        PatientController Patient = new PatientController();
        Models.Patients PatientData = new Models.Patients();
        protected void Page_Load(object sender, EventArgs e)
        {
            DivError.Visible = false;
            DivSuccess.Visible = false;
            string url = "Patients.aspx";
            if (Request.QueryString["ID"] == null)
            {
                Response.Redirect(url);
            }
            int id;
            int.TryParse(Request.QueryString["ID"], out id);
            if (id < 1)
            {
                Response.Redirect(url);
            }
            if (!IsPostBack)
            {
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        protected void MostrarMensaje()
        {
            if (bError)
            {
                DivError.Visible = true;
            }
            else if (bSuccess)
            {
                DivSuccess.Visible = true;
            }
        }
    }
}