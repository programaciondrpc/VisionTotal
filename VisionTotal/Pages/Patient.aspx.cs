﻿using System;
using System.Data;
using VisionTotal.Controllers;

namespace VisionTotal.Pages
{
    public partial class Patient : System.Web.UI.Page
    {
        PatientController PatientContr = new PatientController();
        Models.Patients PatientData = new Models.Patients();
        protected void Page_Load(object sender, EventArgs e)
        {
            string url = "Patients.aspx";
            if (Request.QueryString["ID"] == null)
            {
                Response.Redirect(url);
            }
            int id;
            int.TryParse(Request.QueryString["ID"], out id);
            if (id < 1)
            {
                Response.Redirect(url);
            }
            DataTable results = PatientContr.ViewPatient(id);
            if (results.Rows.Count < 1)
            {
                Response.Redirect(url);
            }
            DateTime DateOfbirth;
            DateTime.TryParse(results.Rows[0]["FechaNacimiento"].ToString(), out DateOfbirth);
            txtName.Text = results.Rows[0]["Nombre"].ToString();
            txtLastname.Text = results.Rows[0]["ApellidoPaterno"].ToString();
            txtLastname2.Text = results.Rows[0]["ApellidoMaterno"].ToString();
            txtStreet.Text = results.Rows[0]["Calle"].ToString();
            txtDistrict.Text = results.Rows[0]["Colonia"].ToString();
            txtIntNumber.Text = results.Rows[0]["NumInt"].ToString();
            txtExtNumber.Text = results.Rows[0]["NumExt"].ToString();
            txtTelephone.Text = results.Rows[0]["Telefono"].ToString();
            txtCellphone.Text = results.Rows[0]["Celular"].ToString();
            txtGender.Text = results.Rows[0]["Genero"].ToString();
            txtDateOfBirth.Text = DateOfbirth.ToString("MM/dd/yyyy");
            txtMaritalStatus.Text = results.Rows[0]["EstadoCivil"].ToString();
            txtEmail.Text = results.Rows[0]["Email"].ToString();
            txtWorkplaceAddress.Text = results.Rows[0]["Trabajo_Direccion"].ToString();
            txtRefference.Text = results.Rows[0]["Referencia"].ToString();
            txtHealthInsurance.Text = results.Rows[0]["Seguro_Medico"].ToString();
        }

        protected void btnConsult_Click(object sender, EventArgs e)
        {
            Response.Redirect("Patient_Consult.aspx?ID=" + Request.QueryString["ID"]);
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Response.Redirect("Patient_Update.aspx?ID=" + Request.QueryString["ID"]);
        }
    }
}