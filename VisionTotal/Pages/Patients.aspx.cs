﻿using System;
using VisionTotal.Controllers;

namespace VisionTotal.Pages
{
    public partial class Patients : System.Web.UI.Page
    {
        public string Html_Table = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PatientController Patient = new PatientController();
                Html_Table = Patient.WriteTable();
            }
        }
    }
}