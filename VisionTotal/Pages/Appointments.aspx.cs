﻿using System;
using VisionTotal.Controllers;
using VisionTotal.Models;

namespace VisionTotal.Pages
{

    public partial class Appointments : System.Web.UI.Page
    {
        public string TitlePage = "Citas del dia ";
        public string Html_Table = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime Day = new DateTime();
                if (Request.QueryString["Date"] == null)
                {
                    TitlePage += "de hoy";
                }
                else
                {

                    DateTime.TryParse(Request.QueryString["Date"].ToString(), out Day);
                    if (Day.Year >= 2000)
                    {
                        string sDate = GeneralTools.DateToString(Day);
                        if (sDate != null)
                        {
                            TitlePage += sDate;
                        }
                    }
                    else
                    {
                        TitlePage += "de hoy";
                    }
                }
                AppointmentController Appointment;
                if (Day.Year >= 2000)
                {
                    Appointment = new AppointmentController(Request.QueryString["Date"]);
                }
                else
                {
                    Appointment = new AppointmentController();
                }
                Html_Table = Appointment.WriteTable();

            }
        }

        protected void btnSearchAnotherDay_Click(object sender, EventArgs e)
        {
            string date = txtDate.Text;
            if (!string.IsNullOrEmpty(date) && date.Length == 10)
            {
                int day, month, year;
                int.TryParse(date.Substring(0, 2), out month);
                int.TryParse(date.Substring(3, 2), out day);
                int.TryParse(date.Substring(6, 4), out year);
                DateTime Date = new DateTime(year, month, day);
                Response.Redirect("Appointments.aspx" + "?Date=" + Date.ToString("MM/dd/yyyy"));
            }
        }
    }
}