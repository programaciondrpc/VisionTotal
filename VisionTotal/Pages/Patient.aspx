﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterTemplate.Master" AutoEventWireup="true" CodeBehind="Patient.aspx.cs" Inherits="VisionTotal.Pages.Patient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
    Paciente
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Subtitle" runat="server">
    Datos
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Body" runat="server">
     <div class="row">
        <div class="col-xs-12 text-left">
            <a href="Patients.aspx" title="Regresar...">
                <div class="col-xs-1 col-sm-1 col-md-1  col-lg-1">
                    <i class="fa fa-long-arrow-left fa-2x visible-xs" aria-hidden="true"></i>
                    <i class="fa fa-long-arrow-left fa-3x visible-sm" aria-hidden="true"></i>
                    <i class="fa fa-long-arrow-left fa-4x hidden-xs hidden-sm" aria-hidden="true"></i>
                </div>
                <div class="col-xs-10 col-sm-3 col-md-3  col-lg-3">
                    <span class="visible-xs" style="font-size: large; vertical-align: text-top;">Regresar</span>
                    <span class="visible-sm" style="font-size: x-large; vertical-align: text-top;">Regresar</span>
                    <span class="hidden-xs hidden-sm" style="font-size: xx-large; vertical-align: text-top;">Regresar</span>
                </div>
            </a>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" style="font-size: 18px;">Datos</div>
        <div class=" panel-body">
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Nombre:*</label>
                        <asp:TextBox Enabled="false" ID="txtName" placeholder="Nombre" CssClass=" form-control text-capitalize" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Apellido paterno:*</label>
                        <asp:TextBox Enabled="false" ID="txtLastname" placeholder="Apellido paterno" CssClass=" form-control text-capitalize" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Apellido materno:</label>
                        <asp:TextBox Enabled="false" ID="txtLastname2" placeholder="Apellido materno" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>


            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Teléfono:</label>
                        <asp:TextBox Enabled="false" ID="txtTelephone" placeholder="Teléfono" CssClass=" form-control  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Celular:</label>
                        <asp:TextBox Enabled="false" ID="txtCellphone" placeholder="Celular" CssClass=" form-control  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label>Genero:</label>
                        <asp:TextBox Enabled="false" ID="txtGender" placeholder="Genero" CssClass=" form-control " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Fecha de nacimiento:</label>
                        <asp:TextBox Enabled="false" ID="txtDateOfBirth" placeholder="Fecha de nacimiento" CssClass=" form-control datepicker " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label>Estado Civil:</label>
                        <asp:TextBox Enabled="false" ID="txtMaritalStatus" placeholder="Estado Civil" CssClass=" form-control   " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>E-mail:</label>
                        <asp:TextBox Enabled="false" ID="txtEmail" placeholder="E-mail" CssClass=" form-control text-lowercase  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Dirección de su trabajo:</label>
                        <asp:TextBox Enabled="false" ID="txtWorkplaceAddress" placeholder="Dirección de su trabajo" CssClass=" form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Quien lo refirio con nosotros:</label>
                        <asp:TextBox Enabled="false" ID="txtRefference" placeholder="Referencia" CssClass=" form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Seguro medico:</label>
                        <asp:TextBox Enabled="false" ID="txtHealthInsurance" placeholder="Seguro medico" CssClass=" form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <a href='javascript:;' data-toggle='collapse' data-target='#Address' class=" text-uppercase btn btn-info MarginBottom10px">Datos de Domicilio </a>
                    </div>
                </div>
            </div>

            <div id="Address" class="collapse">

                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label>Calle:</label>
                            <asp:TextBox Enabled="false" ID="txtStreet" CssClass="form-control text-capitalize" placeholder="Calle" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label>No. Exterior:</label>
                            <asp:TextBox Enabled="false" CssClass="form-control" ID="txtExtNumber" placeholder="No. Exterior" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label>No. Interior:</label>
                            <asp:TextBox Enabled="false" CssClass="form-control" ID="txtIntNumber" placeholder="No. Interior" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label>Colonia:</label>
                            <asp:TextBox Enabled="false" ID="txtDistrict" placeholder="Colonia" CssClass=" form-control text-capitalize" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class=" panel-footer  text-right">
            <asp:Button ID="btnUpdate" runat="server" Text=" Modificar datos" CssClass="btn  btn-default text-uppercase" OnClick="btnUpdate_Click" />
            <asp:Button ID="btnConsult" runat="server" Text=" Consultar" CssClass="btn  btn-info text-uppercase" OnClick="btnConsult_Click" />

        </div>
    </div>
    <%--   <div class="panel panel-default " runat="server">
            <div class="panel-heading" style="font-size: 18px;">Expediente médico</div>
            <div class=" panel-body">

                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <label>Esfera:</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label>Ojo Izquierdo:</label>
                            <asp:TextBox Enabled="false" CssClass="form-control" ID="txtEsferaLeft" placeholder="Ojo Izquierdo" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label>Ojo Derecho:</label>
                            <asp:TextBox Enabled="false" CssClass="form-control" ID="txtEsferaRight" placeholder="Ojo Derecho" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <label>Cilindro:</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label>Ojo Izquierdo:</label>
                            <asp:TextBox Enabled="false" CssClass="form-control" ID="txtCilindroLeft" placeholder="Ojo Izquierdo" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label>Ojo Derecho:</label>
                            <asp:TextBox Enabled="false" CssClass="form-control" ID="txtCilindroRight" placeholder="Ojo Derecho" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <label>Eje:</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label>Ojo Izquierdo:</label>
                            <asp:TextBox Enabled="false" CssClass="form-control" ID="txtEjeLeft" placeholder="Ojo Izquierdo" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label>Ojo Derecho:</label>
                            <asp:TextBox Enabled="false" CssClass="form-control" ID="txtEjeRight" placeholder="Ojo Derecho" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <label>Queratometria:</label>
                            <asp:TextBox Enabled="false" ID="txtQueratometria" placeholder="Queratometria" CssClass=" form-control text-capitalize" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <label>Estereopsis</label>
                            <asp:TextBox Enabled="false" ID="txtEstereopsis" placeholder="Estereopsis" CssClass=" form-control text-capitalize" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <label>Colores:</label>
                            <asp:TextBox Enabled="false" ID="txtColores" placeholder="Colores" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <label>Motilidad ocular:</label>
                            <asp:TextBox Enabled="false" ID="txtMotilidad" placeholder="Motilidad ocular" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <label>Tonapen:</label>
                            <asp:TextBox Enabled="false" ID="txtTonapen" placeholder="Tonapen" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <label>Biomicroscopia:</label>
                            <asp:TextBox Enabled="false" ID="txtBiomicroscopia" placeholder="Biomicroscopia" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <label>Fondo de ojo:</label>
                            <asp:TextBox Enabled="false" ID="txtFondoDeOjo" placeholder="Fondo de ojo" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <label>Alergias:</label>
                            <asp:TextBox Enabled="false" ID="txtAlergias" placeholder="Alergias" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <label>Enfermedades:</label>
                            <asp:TextBox Enabled="false" ID="txtEnfermedades" placeholder="Enfermedades" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <label>Enfermedades hereditarias:</label>
                            <asp:TextBox Enabled="false" ID="txtEnfermedadesHereditarias" placeholder="Enfermedades hereditarias" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <label>Medicamentos:</label>
                            <asp:TextBox Enabled="false" ID="txtMedicamentos" placeholder="Medicamentos" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <label>Diagnostico:</label>
                            <asp:TextBox Enabled="false" ID="txtDiagnostico" placeholder="Diagnostico" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <label>Comentarios:</label>
                            <asp:TextBox Enabled="false" ID="txtComentarios" placeholder="Comentarios" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>

            </div>
            <div class=" panel-footer  text-right">
                <asp:Button ID="Button1" runat="server" Text=" Registrar" CssClass="btn  btn-default text-uppercase" />
            </div>
        </div>--%>
</asp:Content>
