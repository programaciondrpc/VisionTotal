﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterTemplate.Master" AutoEventWireup="true" CodeBehind="Patient_Consult.aspx.cs" Inherits="VisionTotal.Pages.Patient_Consult" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
    Paciente
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Subtitle" runat="server">
    Consulta
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Body" runat="server">
    <div class="alert alert-danger" id="DivError" runat="server" role="alert">
        <%Response.Write(Error); %>
    </div>
    <div class="alert alert-success" id="DivSuccess" runat="server" role="alert">
        <%Response.Write(Success); %>
    </div>
    <div class="row">
        <div class="col-xs-12 text-left">
            <a href="Patient.aspx?ID=<% Response.Write(ID); %>" title="Regresar...">
                <div class="col-xs-1 col-sm-1 col-md-1  col-lg-1">
                    <i class="fa fa-long-arrow-left fa-2x visible-xs" aria-hidden="true"></i>
                    <i class="fa fa-long-arrow-left fa-3x visible-sm" aria-hidden="true"></i>
                    <i class="fa fa-long-arrow-left fa-4x hidden-xs hidden-sm" aria-hidden="true"></i>
                </div>
                <div class="col-xs-10 col-sm-3 col-md-3  col-lg-3">
                    <span class="visible-xs" style="font-size: large; vertical-align: text-top;">Regresar</span>
                    <span class="visible-sm" style="font-size: x-large; vertical-align: text-top;">Regresar</span>
                    <span class="hidden-xs hidden-sm" style="font-size: xx-large; vertical-align: text-top;">Regresar</span>
                </div>
            </a>
        </div>
    </div>
    <div class="panel panel-default " runat="server">
        <div class="panel-heading" style="font-size: 18px;">Expediente médico</div>
        <div class=" panel-body">

            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Razón de la visita:</label>
                        <asp:TextBox ID="txtReason" style="resize:none;" placeholder="Razón de la visita" Rows="3" TextMode="MultiLine" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>

            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Esfera:</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label>Ojo Izquierdo:</label>
                        <asp:TextBox CssClass="form-control" ID="txtEsferaLeft" placeholder="Ojo Izquierdo" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label>Ojo Derecho:</label>
                        <asp:TextBox CssClass="form-control" ID="txtEsferaRight" placeholder="Ojo Derecho" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Cilindro:</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label>Ojo Izquierdo:</label>
                        <asp:TextBox CssClass="form-control" ID="txtCilindroLeft" placeholder="Ojo Izquierdo" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label>Ojo Derecho:</label>
                        <asp:TextBox CssClass="form-control" ID="txtCilindroRight" placeholder="Ojo Derecho" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Eje:</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label>Ojo Izquierdo:</label>
                        <asp:TextBox CssClass="form-control" ID="txtEjeLeft" placeholder="Ojo Izquierdo" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label>Ojo Derecho:</label>
                        <asp:TextBox CssClass="form-control" ID="txtEjeRight" placeholder="Ojo Derecho" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>

            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Queratometria:</label>
                        <asp:TextBox ID="txtQueratometria" placeholder="Queratometria" CssClass=" form-control text-capitalize" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Estereopsis</label>
                        <asp:TextBox ID="txtEstereopsis" placeholder="Estereopsis" CssClass=" form-control text-capitalize" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Colores:</label>
                        <asp:TextBox ID="txtColores" placeholder="Colores" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>

            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Motilidad ocular:</label>
                        <asp:TextBox ID="txtMotilidad" placeholder="Motilidad ocular" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Tonapen:</label>
                        <asp:TextBox ID="txtTonapen" placeholder="Tonapen" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Biomicroscopia:</label>
                        <asp:TextBox ID="txtBiomicroscopia" placeholder="Biomicroscopia" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Fondo de ojo:</label>
                        <asp:TextBox ID="txtFondoDeOjo" placeholder="Fondo de ojo" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Alergias:</label>
                        <asp:TextBox ID="txtAlergias" placeholder="Alergias" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Enfermedades:</label>
                        <asp:TextBox ID="txtEnfermedades" placeholder="Enfermedades" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Enfermedades hereditarias:</label>
                        <asp:TextBox ID="txtEnfermedadesHereditarias" placeholder="Enfermedades hereditarias" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Medicamentos:</label>
                        <asp:TextBox ID="txtMedicamentos" placeholder="Medicamentos" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Diagnostico:</label>
                        <asp:TextBox ID="txtDiagnostico" Rows="3" style="resize:none;" TextMode="MultiLine"  placeholder="Diagnostico" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <label>Comentarios:</label>
                        <asp:TextBox ID="txtComentarios" style="resize:none;" Rows="3" TextMode="MultiLine"  placeholder="Comentarios" CssClass=" form-control text-capitalize  " runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>

        </div>
        <div class=" panel-footer  text-right">
            <asp:Button ID="btnSave" runat="server" Text=" Guardar consulta" CssClass="btn  btn-default text-uppercase" OnClick="btnSave_Click" />
        </div>
    </div>
</asp:Content>
