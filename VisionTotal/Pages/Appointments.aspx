﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterTemplate.Master" AutoEventWireup="true" CodeBehind="Appointments.aspx.cs" Inherits="VisionTotal.Pages.Appointments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Title" runat="server">
    <%  Response.Write(TitlePage);%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Subtitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" runat="server">
    <div class="row">
        <div class=" col-xs-offset-8 col-xs-4 col-sm-offset-9 col-sm-3 
            col-md-offset-9 col-md-3 col-lg-offset-9 col-lg-3 text-right">
            <a href="Reports.aspx" title="Reportes">
                <i class="fa fa-bar-chart fa-2x visible-xs" aria-hidden="true"></i>
                <i class="fa fa-bar-chart fa-3x visible-sm" aria-hidden="true"></i>
                <i class="fa fa-bar-chart fa-4x hidden-xs hidden-sm" aria-hidden="true"></i>
            </a>
        </div>
    </div>
    <div class="row">
        <% Response.Write(Html_Table); %>
    </div>
    <br />
    <div class="row">
        <div class="col-xs-9 col-sm-offset-4 col-sm-3 col-md-offset-4 col-md-3">
            <asp:TextBox ID="txtDate" placeholder="Ver otro día" CssClass=" form-control datepicker text-right" runat="server" />
        </div>
        <div class="col-xs-3 col-sm-3 col-md-2">
            <asp:Button ID="btnSearchAnotherDay" OnClick="btnSearchAnotherDay_Click"  ToolTip="Buscar citas de otro día..." CssClass="btn btn-info" runat="server" Text="Ir" />
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-xs-6 col-sm-3 text-left">
              <a href="Patients.aspx" title="Pacientes">
                <i class="fa fa-users fa-2x visible-xs" aria-hidden="true"></i>
                <i class="fa fa-users fa-3x visible-sm" aria-hidden="true"></i>
                <i class="fa fa-users fa-4x hidden-xs hidden-sm" aria-hidden="true"></i>
            </a>
        </div>
          <div class="col-xs-6  col-sm-offset-6 col-sm-3 col-md-offset-6 col-md-3
              col-lg-offset-6 col-lg-3 text-right">
              <a href="Appointment.aspx" title="Citas">
                <i class="fa fa-calendar fa-2x visible-xs" aria-hidden="true"></i>
                <i class="fa fa-calendar fa-3x visible-sm" aria-hidden="true"></i>
                <i class="fa fa-calendar fa-4x hidden-xs hidden-sm" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</asp:Content>
