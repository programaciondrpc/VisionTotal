﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterTemplate.Master" AutoEventWireup="true" CodeBehind="Patients.aspx.cs" Inherits="VisionTotal.Pages.Patients" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Title" runat="server">
    Pacientes
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Subtitle" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Body" runat="server">
    <div class="row">
        <div class="col-xs-12 text-left">
            <a href="Appointments.aspx" title="Regresar...">
                <div class="col-xs-1 col-sm-1 col-md-1  col-lg-1">
                    <i class="fa fa-long-arrow-left fa-2x visible-xs" aria-hidden="true"></i>
                    <i class="fa fa-long-arrow-left fa-3x visible-sm" aria-hidden="true"></i>
                    <i class="fa fa-long-arrow-left fa-4x hidden-xs hidden-sm" aria-hidden="true"></i>
                </div>
                <div class="col-xs-10 col-sm-3 col-md-3  col-lg-3">
                    <span class="visible-xs" style="font-size: large; vertical-align: text-top;">Regresar</span>
                    <span class="visible-sm" style="font-size: x-large; vertical-align: text-top;">Regresar</span>
                    <span class="hidden-xs hidden-sm" style="font-size: xx-large; vertical-align: text-top;">Regresar</span>
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <% Response.Write(Html_Table); %>
    </div>
    <br />
    <div class="row">
        <div class="col-xs-6 col-sm-3 text-left">
            <a href="Patient_ADD.aspx" title="Agregar paciente">
                <i class="fa fa-user-plus fa-2x visible-xs" aria-hidden="true"></i>
                <i class="fa fa-user-plus fa-3x visible-sm" aria-hidden="true"></i>
                <i class="fa fa-user-plus fa-4x hidden-xs hidden-sm" aria-hidden="true"></i>
            </a>
        </div>
        <div class="col-xs-6  col-sm-offset-6 col-sm-3 col-md-offset-6 col-md-3
              col-lg-offset-6 col-lg-3 text-right">
            <a href="Appointment_ADD.aspx" title="Agregar cita">
                <i class="fa fa-calendar-plus-o fa-2x visible-xs" aria-hidden="true"></i>
                <i class="fa fa-calendar-plus-o fa-3x visible-sm" aria-hidden="true"></i>
                <i class="fa fa-calendar-plus-o fa-4x hidden-xs hidden-sm" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</asp:Content>
