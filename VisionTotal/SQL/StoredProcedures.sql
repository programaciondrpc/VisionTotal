/*Lista de Citas por dia*/
alter PROCEDURE [dbo].[ViewAppointmentsOfDate]
(@FechaProgramada Datetime)
AS
select Cita_ID,
(Nombre + ' '+ ApellidoPaterno+ ' '+ ApellidoMaterno) as Paciente, 
RazonVisita = 'Razón de Visita',
Confirmacion as 'Confirmación',
Seguro_Medico as 'Seguro medico'
from [dbo].Citas 
inner join 
[dbo].Pacientes on [dbo].Citas.Paciente_id = [dbo].Pacientes.Paciente_ID
where FechaProgramada = @FechaProgramada
return 
GO

/*Lista de correos para notificaciones*/
alter PROCEDURE [dbo].[SearchNotificationsEmails]
AS
select * from [dbo].[Notificaciones] 
return 
GO


/*Lista de Pacientes*/
alter PROCEDURE [dbo].[ViewPatients]
AS
select Paciente_ID, 
(Nombre + ' '+ ApellidoPaterno+ ' '+ ApellidoMaterno) as Paciente, 
Genero,
Telefono,
Celular,
Seguro_Medico as 'Seguro medico'
from [dbo].Pacientes
inner join [dbo].Generos on [dbo].Pacientes.Genero_ID = [dbo].Generos.Genero_ID
return 
GO

/*Lista de generos*/
create PROCEDURE [dbo].[ViewGenders]
AS
select Genero_ID, Genero
from [dbo].generos
return 
GO


/*Lista de Estados civiles*/
create PROCEDURE [dbo].[ViewMaritalStatus]
AS
select EstadoCivil_ID, EstadoCivil
from [dbo].Estadosciviles
return 
GO


/*Insertar pacientes*/
alter PROCEDURE [dbo].[InsertPatient]
(
@Nombre varchar(100),
@ApellidoPaterno varchar(50),
@ApellidoMaterno varchar(50),
@Calle varchar(50),
@NumExt varchar(10),
@NumInt varchar(10),
@Colonia varchar(50),
@Genero_ID int,
@EstadoCivil_ID int,
@Email varchar(100),
@Telefono varchar(20),
@Celular varchar(20),
@FechaNacimiento datetime,
@Trabajo_Direccion varchar(100),
@Referencia varchar(100),
@Seguro_Medico Varchar(50)
)
AS
insert into Pacientes (Nombre,ApellidoPaterno,ApellidoMaterno,Calle,NumExt,NumInt,Colonia,Genero_ID,EstadoCivil_ID,Email,Telefono,Celular,Trabajo_Direccion,FechaNacimiento,Referencia,Seguro_Medico)
values (@Nombre,@ApellidoPaterno,@ApellidoMaterno,@Calle,@NumExt,@NumInt,@Colonia,@Genero_ID,@EstadoCivil_ID,@Email,@Telefono,@Celular,@Trabajo_Direccion,@FechaNacimiento,@Referencia,@Seguro_Medico)
--Declare @Id_usuario  int
SELECT IDENT_CURRENT('Pacientes') as Paciente_ID
return 
GO

/*Ver Paciente*/
alter PROCEDURE [dbo].[ViewPatient]
(@Paciente_ID int )
AS
select [dbo].Pacientes.*,Genero,EstadoCivil
from [dbo].Pacientes inner join
[dbo].Generos on [dbo].Generos.Genero_ID = [dbo].Pacientes.Genero_ID
inner join
[dbo].EstadosCiviles on [dbo].EstadosCiviles.EstadoCivil_ID = [dbo].Pacientes.EstadoCivil_ID
where Paciente_ID = @Paciente_ID
return 
GO

select * from pacientes