/* Tabla Generos*/
create table [dbo].[Generos](
Genero_ID int identity (1,1) primary key,
Genero varchar (50)
)
go
insert into [dbo].[Generos] (Genero) values('Masculino'),('Femenino')

/* Tabla Estados civiles*/
create table [dbo].[EstadosCiviles](
EstadoCivil_ID int identity (1,1) primary key,
EstadoCivil varchar (50)
)
go
insert into [dbo].[EstadosCiviles] (EstadoCivil) values('Soltero'),('Casado'),
('Viudo'),('Divorciado'),('Uni�n Libre'),('Comprometido')

/* Tabla Notificaciones*/
create table [dbo].[Notificaciones]
(
Email_ID int identity (1,1) primary key,
Email varchar(100)
)
insert into [Notificaciones] (Email)
values ('fernandopedroza@outlook.com')
go

/* Tabla Pacientes*/
create table [dbo].[Pacientes]
(
Paciente_ID int identity (1,1) primary key,
Nombre varchar(100),
ApellidoPaterno varchar(50),
ApellidoMaterno varchar(50),
Calle varchar(50),
NumExt varchar(10),
NumInt varchar(10),
Colonia varchar(50),
FechaNacimiento datetime,
Genero_ID int,
EstadoCivil_ID int,
Email varchar(100),
Telefono varchar(20),
Celular varchar(20),
Trabajo_Direccion varchar(100),
Referencia varchar(100),
Seguro_Medico Varchar(50),
foreign key (Genero_ID) references Generos(Genero_ID),
foreign key (EstadoCivil_ID) references EstadosCiviles(EstadoCivil_ID),
)
go

alter table [Pacientes]
add  FechaNacimiento datetime

/* Tabla Expedientes*/
create table [dbo].[Expedientes](
Historial_ID int identity (1,1) primary key,
Paciente_ID int,
OjoDerecho_Esfera varchar(30),
OjoIzquierdo_Esfera varchar(30),
OjoDerecho_Cilindro varchar(30),
OjoIzquierdo_Cilindro varchar(30),
OjoDerecho_Eje varchar(100),
OjoIzquierdo_Eje varchar(100),
Queratometria varchar(100),
Estereopsis varchar(100),
Colores varchar(100),
MotilidadOcular varchar(100),
Tonapen varchar(100),
Biomicroscopia varchar(100),
FondoDeOjo varchar(100),
Alergias varchar(100),
Enfermedades varchar(100),
EnfermedadesHereditarias varchar(100),
Medicamentos varchar(100),
Diagnostico varchar(200),
Comentarios varchar(300),
foreign key (Paciente_ID) references Pacientes(Paciente_ID),
)
go

/* Tabla Citas*/
create table [dbo].[Citas](
Cita_ID int identity (1,1) primary key,
Paciente_ID int,
FechaProgramada Datetime,
RazonVisita varchar(200),
Confirmacion bit,
Nota varchar(200),
foreign key (Paciente_ID) references Pacientes(Paciente_ID),
)
go