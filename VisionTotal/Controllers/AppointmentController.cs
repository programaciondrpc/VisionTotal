﻿using System;
using System.Data;
using System.Data.SqlClient;
using VisionTotal.Models;

namespace VisionTotal.Controllers
{
    public class AppointmentController
    {
        #region Propiedades
        public DateTime Date { get; set; }
        #endregion


        #region Constructors
        public AppointmentController(string sdate)
        {
            DateTime date;
            if (DateTime.TryParse(sdate, out date))
            {
                Date = date;
            }
            else
            {
                Date = DateTime.Now;
            }
           
        }
        public AppointmentController()
        {
            Date = DateTime.Now;
        }
        #endregion

        #region Consultas
        private DataTable ViewList()
        {
            DataTable table = new DataTable();
            SqlCommand cmdselect = new SqlCommand();
            cmdselect.CommandText = "ViewAppointmentsOfDate";
            cmdselect.Parameters.Add("@FechaProgramada", SqlDbType.DateTime).Value = Date;
            table = DataContext.select(cmdselect);            
            return table;
        }
        #endregion


        #region HTML

        public string WriteTable() {

            string Code = ""; // codigo para tabla
            string Link = "Appointment.aspx?ID="; // link para hipervinculo en tabla
            int ID = 0; //Numero de Columna de la tabla
            int Start = 1;//Apartir de esta columna empezara la tabla
            bool Toggle = true;  //Opcion para cambiar vista de columnas
            bool ShowColumns = true; //Opcion Cambiar Columnas
            bool Search = true;  //Opcion para buscar
            bool Pagination = true; //Opcion para paginar datos
            bool Export = true; //Opcion para exportar datos
            DataTable table = ViewList(); // datos de consulta
            if (table==null)
            {
                return "Error en la consulta de datos...";
            }
            HTML html = new HTML();
            Code = html.WriteTabla(table, Link, ID, Start, Toggle, ShowColumns, Search, Pagination, Export);
            return Code;
        }
        #endregion

    }
}