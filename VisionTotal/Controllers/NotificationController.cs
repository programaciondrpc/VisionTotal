﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using VisionTotal.Models;

namespace VisionTotal.Controllers
{
    public static class NotificationController
    {
        static string SystemName = "Vision Total";
        public  static string SP_Notifications = "SearchNotificationsEmails";
        public static bool SendSmtp(string To, string Subject, string Body, bool HTML)
        {
            bool Success = false;
            try
            {
                // Crear el Mail
                using (var mail = new MailMessage())
                {
                    mail.To.Add(new MailAddress(To));
                    mail.Subject = Subject;
                    mail.SubjectEncoding = System.Text.Encoding.UTF8;
                    mail.Body = Body;
                    mail.BodyEncoding = System.Text.Encoding.UTF8;
                    if (HTML)
                    {
                        mail.IsBodyHtml = true;
                    }

                    // Configuración SMTP los valores los saca del web.config
                    new SmtpClient().Send(mail);
                    Success = true;
                } // end using mail
            }
            catch (Exception ex)
            {
                Success = false;
            }
            return Success;
        } // end SMTPMail

        public static bool SendNotifications(string Subject, string Body)
        {
            DataTable table = new DataTable();
            bool error = false;
            SqlCommand cmdselect = new SqlCommand();
            cmdselect.CommandText = "BuscarEmailsNotificaciones";
            table = DataContext.select(cmdselect);
            if (table != null)
            {
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    if (!SendSmtp(table.Rows[i]["Email"].ToString(), Subject, Body, false))
                    {
                        error = true;
                    }
                }
            }
            else
            {
                error = true;
            }
            return error;
        }

        public static bool SendNotificationsErrorTemplate(string errorMessage,string exception, DateTime datetime )
        {
            DataTable table = new DataTable();
            string subject = "Error en sistema de" + SystemName;
            string body = "Se genero un error \n\n " + errorMessage + "\n\n" + exception + " \n\nHora " + datetime.ToString() + "\n\n";
            bool error = false;
            SqlCommand cmdselect = new SqlCommand();
            cmdselect.CommandText = SP_Notifications;
            table = DataContext.select(cmdselect);
            if (table != null)
            {
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    if (!SendSmtp(table.Rows[i]["Email"].ToString(), subject, body, false))
                    {
                        error = true;
                    }
                }
            }
            else
            {
                error = true;
            }
            return error;
        }

    }
}