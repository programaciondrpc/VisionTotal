﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using VisionTotal.Models;

namespace VisionTotal.Controllers
{
    public class PatientController
    {
        #region Properties

        #endregion


        #region Constructors
        public PatientController()
        {
        }
        #endregion

        #region Consults
        private DataTable ViewList()
        {
            DataTable table = new DataTable();
            SqlCommand cmdselect = new SqlCommand();
            cmdselect.CommandText = "ViewPatients";
            table = DataContext.select(cmdselect);
            return table;
        }

        public DataTable ViewMaritalStatusList()
        {
            DataTable table = new DataTable();
            SqlCommand cmdselect = new SqlCommand();
            cmdselect.CommandText = "ViewMaritalStatus";
            table = DataContext.select(cmdselect);
            return table;
        }

        public DataTable ViewGenderList()
        {
            DataTable table = new DataTable();
            SqlCommand cmdselect = new SqlCommand();
            cmdselect.CommandText = "ViewGenders";
            table = DataContext.select(cmdselect);
            return table;
        }

        public DataTable ViewPatient(int id)
        {
            DataTable table = new DataTable();
            SqlCommand cmdselect = new SqlCommand();
            cmdselect.CommandText = "ViewPatient";
            cmdselect.Parameters.Add("@Paciente_ID", SqlDbType.Int).Value = id;
            table = DataContext.select(cmdselect);
            return table;
        }

        #endregion

        #region Inserts
        public bool Insert(Patients Patient)
        {
            bool Error;
            SqlCommand cmdselect = new SqlCommand();
            cmdselect.CommandText = "InsertPatient";
            cmdselect.Parameters.Add("@Nombre", SqlDbType.VarChar,100).Value = Patient.Nombre;
            cmdselect.Parameters.Add("@ApellidoPaterno", SqlDbType.VarChar,50).Value = Patient.ApellidoPaterno;
            cmdselect.Parameters.Add("@ApellidoMaterno", SqlDbType.VarChar, 50).Value = Patient.ApellidoMaterno;
            cmdselect.Parameters.Add("@Calle", SqlDbType.VarChar, 50).Value = Patient.Calle;
            cmdselect.Parameters.Add("@NumExt", SqlDbType.VarChar, 10).Value = Patient.NumExt;
            cmdselect.Parameters.Add("@NumInt", SqlDbType.VarChar, 10).Value = Patient.NumInt;
            cmdselect.Parameters.Add("@Colonia", SqlDbType.VarChar, 50).Value = Patient.Colonia;
            cmdselect.Parameters.Add("@FechaNacimiento", SqlDbType.DateTime).Value = Patient.FechaNacimiento;
            cmdselect.Parameters.Add("@Genero_ID", SqlDbType.Int).Value = Patient.Genero_ID;
            cmdselect.Parameters.Add("@EstadoCivil_ID", SqlDbType.Int).Value = Patient.EstadoCivil_ID;
            cmdselect.Parameters.Add("@Email", SqlDbType.VarChar, 100).Value = Patient.Email;
            cmdselect.Parameters.Add("@Telefono", SqlDbType.VarChar, 20).Value = Patient.Telefono;
            cmdselect.Parameters.Add("@Celular", SqlDbType.VarChar, 20).Value = Patient.Celular;
            cmdselect.Parameters.Add("@Trabajo_Direccion", SqlDbType.VarChar, 100).Value = Patient.Trabajo_Direccion;
            cmdselect.Parameters.Add("@Referencia", SqlDbType.VarChar, 100).Value = Patient.Referencia;
            cmdselect.Parameters.Add("@Seguro_Medico", SqlDbType.VarChar, 50).Value = Patient.Seguro_Medico;
            Error = DataContext.Insert(cmdselect);
            return Error;
        }
        public DataTable InsertWithResult(Patients Patient)
        {
            DataTable table = new DataTable();
            SqlCommand cmdselect = new SqlCommand();
            cmdselect.CommandText = "InsertPatient";
            cmdselect.Parameters.Add("@Nombre", SqlDbType.VarChar, 100).Value = Patient.Nombre;
            cmdselect.Parameters.Add("@ApellidoPaterno", SqlDbType.VarChar, 50).Value = Patient.ApellidoPaterno;
            cmdselect.Parameters.Add("@ApellidoMaterno", SqlDbType.VarChar, 50).Value = Patient.ApellidoMaterno;
            cmdselect.Parameters.Add("@Calle", SqlDbType.VarChar, 50).Value = Patient.Calle;
            cmdselect.Parameters.Add("@NumExt", SqlDbType.VarChar, 10).Value = Patient.NumExt;
            cmdselect.Parameters.Add("@FechaNacimiento", SqlDbType.DateTime).Value = Patient.FechaNacimiento;
            cmdselect.Parameters.Add("@NumInt", SqlDbType.VarChar, 10).Value = Patient.NumInt;
            cmdselect.Parameters.Add("@Colonia", SqlDbType.VarChar, 50).Value = Patient.Colonia;
            cmdselect.Parameters.Add("@Genero_ID", SqlDbType.Int).Value = Patient.Genero_ID;
            cmdselect.Parameters.Add("@EstadoCivil_ID", SqlDbType.Int).Value = Patient.EstadoCivil_ID;
            cmdselect.Parameters.Add("@Email", SqlDbType.VarChar, 100).Value = Patient.Email;
            cmdselect.Parameters.Add("@Telefono", SqlDbType.VarChar, 20).Value = Patient.Telefono;
            cmdselect.Parameters.Add("@Celular", SqlDbType.VarChar, 20).Value = Patient.Celular;
            cmdselect.Parameters.Add("@Trabajo_Direccion", SqlDbType.VarChar, 100).Value = Patient.Trabajo_Direccion;
            cmdselect.Parameters.Add("@Referencia", SqlDbType.VarChar, 100).Value = Patient.Referencia;
            cmdselect.Parameters.Add("@Seguro_Medico", SqlDbType.VarChar, 50).Value = Patient.Seguro_Medico;
            table = DataContext.select(cmdselect);
            return table;
        }
        #endregion

        #region Update

        public bool Update(Patients Patient)
        {
            bool Error;
            SqlCommand cmdselect = new SqlCommand();
            cmdselect.CommandText = "UpdatePatient";
            cmdselect.Parameters.Add("@Paciente_ID", SqlDbType.Int).Value = Patient.Paciente_ID;
            cmdselect.Parameters.Add("@Nombre", SqlDbType.VarChar, 100).Value = Patient.Nombre;
            cmdselect.Parameters.Add("@ApellidoPaterno", SqlDbType.VarChar, 50).Value = Patient.ApellidoPaterno;
            cmdselect.Parameters.Add("@ApellidoMaterno", SqlDbType.VarChar, 50).Value = Patient.ApellidoMaterno;
            cmdselect.Parameters.Add("@Calle", SqlDbType.VarChar, 50).Value = Patient.Calle;
            cmdselect.Parameters.Add("@NumExt", SqlDbType.VarChar, 10).Value = Patient.NumExt;
            cmdselect.Parameters.Add("@NumInt", SqlDbType.VarChar, 10).Value = Patient.NumInt;
            cmdselect.Parameters.Add("@Colonia", SqlDbType.VarChar, 50).Value = Patient.Colonia;
            cmdselect.Parameters.Add("@FechaNacimiento", SqlDbType.DateTime).Value = Patient.FechaNacimiento;
            cmdselect.Parameters.Add("@Genero_ID", SqlDbType.Int).Value = Patient.Genero_ID;
            cmdselect.Parameters.Add("@EstadoCivil_ID", SqlDbType.Int).Value = Patient.EstadoCivil_ID;
            cmdselect.Parameters.Add("@Email", SqlDbType.VarChar, 100).Value = Patient.Email;
            cmdselect.Parameters.Add("@Telefono", SqlDbType.VarChar, 20).Value = Patient.Telefono;
            cmdselect.Parameters.Add("@Celular", SqlDbType.VarChar, 20).Value = Patient.Celular;
            cmdselect.Parameters.Add("@Trabajo_Direccion", SqlDbType.VarChar, 100).Value = Patient.Trabajo_Direccion;
            cmdselect.Parameters.Add("@Referencia", SqlDbType.VarChar, 100).Value = Patient.Referencia;
            cmdselect.Parameters.Add("@Seguro_Medico", SqlDbType.VarChar, 50).Value = Patient.Seguro_Medico;
            Error = DataContext.Update(cmdselect);
            return Error;
        }
        #endregion

        #region HTML

        public string WriteTable()
        {

            string Code = ""; // codigo para tabla
            string Link = "Patient.aspx?ID="; // link para hipervinculo en tabla
            int ID = 0; //Numero de Columna de la tabla
            int Start = 1;//Apartir de esta columna empezara la tabla
            bool Toggle = true;  //Opcion para cambiar vista de columnas
            bool ShowColumns = true; //Opcion Cambiar Columnas
            bool Search = true;  //Opcion para buscar
            bool Pagination = true; //Opcion para paginar datos
            bool Export = true; //Opcion para exportar datos
            DataTable table = ViewList(); // datos de consulta
            if (table == null)
            {
                return "Error en la consulta de datos...";
            }
            HTML html = new HTML();
            Code = html.WriteTabla(table, Link, ID, Start, Toggle, ShowColumns, Search, Pagination, Export);
            return Code;
        }
        #endregion

    }
}