﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace VisionTotal.Models
{
    public class HTML
    {

        #region Tables
        public string WriteTabla(DataTable table, string link, int id, int start,
    bool Toggle, bool ShowColumns, bool Search, bool Pagination, bool Export)
        {
            string toggle = Toggle.ToString().ToLower();
            string showColumns = ShowColumns.ToString().ToLower();
            string search = Search.ToString().ToLower();
            string pagination = Pagination.ToString().ToLower();
            string export = Export.ToString().ToLower();


            string HTMLTable = @"  <table data-toggle='table' data-show-toggle='" + toggle +
                "' data-show-columns='" + showColumns + "' data-search='" + search +
                "' data-pagination='" + pagination + "'  data-show-export='" + export +
                @"'  class='table table-condensed'>
                    <thead>
                    <tr>";
            for (int i = start; i < table.Columns.Count; i++)
            {
                HTMLTable += "<th data-sortable='true' title='" + table.Columns[i].ColumnName + "'>" + table.Columns[i].ColumnName + "</th>";
            }
            HTMLTable += @"</tr>
                      </thead>
                      <tbody>";
            for (int i = 0; i < table.Rows.Count; i++)
            {
                HTMLTable += " <tr>" + System.Environment.NewLine;
                for (int col = start; col < table.Columns.Count; col++)
                {
                    if (col == start)
                    {
                        HTMLTable += "<td><a title='Ver datos' href='" + link + table.Rows[i][id].ToString() + "'>" +
                         table.Rows[i][id + start].ToString() + "</a></td>" + System.Environment.NewLine;
                    }
                    else
                    {
                        HTMLTable += "<td>" + table.Rows[i][col].ToString() + "</td>" + System.Environment.NewLine;
                    }
                }
                HTMLTable += "</tr>" + System.Environment.NewLine;
            }
            HTMLTable += "</tbody>" + System.Environment.NewLine + "</table>";
            return HTMLTable;
        } 
        #endregion

    }
}