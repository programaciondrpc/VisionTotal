﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisionTotal.Models
{
    public class Appointments
    {
        #region Properties
        public int Cita_ID { get; set; }
        public int Paciente_ID { get; set; }
        public DateTime FechaProgramada { get; set; }
        public bool Confirmacion { get; set; }
        public string Nota { get; set; }

        #endregion
    }
}