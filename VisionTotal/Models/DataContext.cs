﻿using System;
using System.Data;
using System.Data.SqlClient;
using VisionTotal.Controllers;

namespace VisionTotal.Models
{
    public static class DataContext
    {
        private static SqlConnection  Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);



        #region Select Data
        public static DataTable select(SqlCommand cmdselectaux)
        {
            SqlDataReader myReader;
            DataTable table = new DataTable();
            try
            {
                //se abre la conexion a la base de datos
                Connection.Open();
                SqlCommand cmdselect = Connection.CreateCommand();
                for (int i = 0; i < cmdselectaux.Parameters.Count; i++)
                {
                    cmdselect.Parameters.Add(cmdselectaux.Parameters[i].ParameterName,
                   cmdselectaux.Parameters[i].SqlDbType).Value = cmdselectaux.Parameters[i].Value;
                }
                cmdselect.CommandText = cmdselectaux.CommandText;
                cmdselect.CommandType = CommandType.StoredProcedure;
                myReader = cmdselect.ExecuteReader();
                if (myReader != null)
                {
                    table.Load(myReader);
                }
            }
            catch (SqlException ex)
            {
                Connection.Close();
                if (!ex.ToString().Contains(NotificationController.SP_Notifications))
                {
                    NotificationController.SendNotificationsErrorTemplate(ex.Message, ex.ToString(), DateTime.Now);
                }
                return null;
            }
            finally
            {
                Connection.Close();    //se cierra la conexion para liberar espacio en memoria
            }
            return table;
        }

        #endregion

        #region Insert Data

        public static bool Insert(SqlCommand cmdselectaux)
        {
            //SqlTransaction mytran = null;
            try
            {
                //se abre la conexion a la base de datos
                Connection.Open();
                //mytran = Connection.BeginTransaction();
                SqlCommand cmdselect = Connection.CreateCommand();
                for (int i = 0; i < cmdselectaux.Parameters.Count; i++)
                {
                    cmdselect.Parameters.Add(cmdselectaux.Parameters[i].ParameterName,
                   cmdselectaux.Parameters[i].SqlDbType).Value = cmdselectaux.Parameters[i].Value;
                }
                cmdselect.CommandText = cmdselectaux.CommandText;
                cmdselect.CommandType = CommandType.StoredProcedure;
                cmdselect.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Connection.Close();
                if (!ex.ToString().Contains(NotificationController.SP_Notifications))
                {
                    NotificationController.SendNotificationsErrorTemplate(ex.Message, ex.ToString(), DateTime.Now);
                }
                return true;
            }
            finally
            {
                Connection.Close();    //se cierra la conexion para liberar espacio en memoria
            }
            return false;
        }

        #endregion

        #region Update Data

        public static bool Update(SqlCommand cmdselectaux)
        {
            //SqlTransaction mytran = null;
            try
            {
                //se abre la conexion a la base de datos
                Connection.Open();
                //mytran = Connection.BeginTransaction();
                SqlCommand cmdselect = Connection.CreateCommand();
                for (int i = 0; i < cmdselectaux.Parameters.Count; i++)
                {
                    cmdselect.Parameters.Add(cmdselectaux.Parameters[i].ParameterName,
                   cmdselectaux.Parameters[i].SqlDbType).Value = cmdselectaux.Parameters[i].Value;
                }
                cmdselect.CommandText = cmdselectaux.CommandText;
                cmdselect.CommandType = CommandType.StoredProcedure;
                cmdselect.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Connection.Close();
                if (!ex.ToString().Contains(NotificationController.SP_Notifications))
                {
                    NotificationController.SendNotificationsErrorTemplate(ex.Message, ex.ToString(), DateTime.Now);
                }
                return true;
            }
            finally
            {
                Connection.Close();    //se cierra la conexion para liberar espacio en memoria
            }
            return false;
        }

        #endregion
    }
}