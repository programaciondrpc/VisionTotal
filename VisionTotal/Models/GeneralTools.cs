﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisionTotal.Models
{
    public static class GeneralTools
    {
        public static string MonthToString(int month)
        {
            string Month;
            switch (month)
            {
                case 1:Month = "Enero";
                    break;
                case 2:
                    Month = "Febrero";
                    break;
                case 3:
                    Month = "Marzo";
                    break;
                case 4:
                    Month = "Abril";
                    break;
                case 5:
                    Month = "Mayo";
                    break;
                case 6:
                    Month = "Junio";
                    break;
                case 7:
                    Month = "Julio";
                    break;
                case 8:
                    Month = "Agosto";
                    break;
                case 9:
                    Month = "Septiembre";
                    break;
                case 10:
                    Month = "Octubre";
                    break;
                case 11:
                    Month = "Noviembre";
                    break;
                case 12:
                    Month = "Diciembre";
                    break;
                default:
                    Month = null;
                    break;
            }
            return Month;
        }

        public static string DateToString(DateTime date)
        {
            string Date;

            Date = date.Day.ToString("00");
            Date += " de ";
            string month = MonthToString(date.Month);
            if (month == null)
            {
                return null;
            }
            Date += month;
            Date += " del ";
            Date += date.Year.ToString("0000");
            return Date;
        }
    }
}