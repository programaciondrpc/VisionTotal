﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisionTotal.Models
{
    public class Patients
    {
        #region Properties
        public int Paciente_ID { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Calle { get; set; }
        public string NumExt { get; set; }
        public string NumInt { get; set; }
        public string Colonia { get; set; }
        public int Genero_ID { get; set; }
        public int EstadoCivil_ID { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Trabajo_Direccion { get; set; }
        public string Referencia { get; set; }
        public string Seguro_Medico { get; set; }
        public DateTime FechaNacimiento { get; set; }

        #endregion
    }
}