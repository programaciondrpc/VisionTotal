﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisionTotal.Models
{
    public class Consultations
    {
        #region Properties
        public int Historial_ID { get; set; }
        public int Paciente_ID { get; set; }
        public string OjoDerecho_Esfera { get; set; }
        public string OjoIzquierdo_Esfera { get; set; }
        public string OjoDerecho_Cilindro { get; set; }
        public string OjoIzquierdo_Cilindro { get; set; }
        public string OjoDerecho_Eje { get; set; }
        public string OjoIzquierdo_Eje { get; set; }
        public string Queratometria { get; set; }
        public string Estereopsis { get; set; }
        public string Colores { get; set; }
        public string MotilidadOcular { get; set; }
        public string Tonapen { get; set; }
        public string Biomicroscopia { get; set; }
        public string FondoDeOjo { get; set; }
        public string Alergias { get; set; }
        public string Enfermedades { get; set; }
        public string EnfermedadesHereditarias { get; set; }
        public string Medicamentos { get; set; }
        public string Diagnostico { get; set; }
        public string Comentarios { get; set; }
        #endregion

    }
}